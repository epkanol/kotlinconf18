package net.sarazan.sampleplugin

import android.net.Uri
import net.sarazan.plugin.Plugin
import net.sarazan.plugin.handlers.PluginUriHandler

val pingHandler = object : PluginUriHandler {
    override fun handle(plugin: Plugin, uri: Uri): Boolean {
        plugin.host.alerts.toast("PING!")
        return true
    }
}