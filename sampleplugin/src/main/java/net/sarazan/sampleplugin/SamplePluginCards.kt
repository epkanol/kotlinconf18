package net.sarazan.sampleplugin

import kotlinx.android.synthetic.main.card_sample_plugin.view.*
import net.sarazan.plugin.cards.PluginCardVH
import net.sarazan.plugin.cards.PluginCards

class SamplePluginCards : PluginCards() {

    override fun layout(): Int = R.layout.card_sample_plugin

    override fun onBindViewHolder(p0: PluginCardVH) {
        p0.itemView.button.setOnClickListener {
            plugin.alerts?.show(R.id.sample_alert_1)
        }
    }
}