package net.sarazan.sampleplugin

import net.sarazan.plugin.*

val samplePlugin = plugin("net.sarazan.sample1") {

    cards = SamplePluginCards()

    alerts {

        toasts {
            allow("This is an approved message.")
        }

        alert(R.id.sample_alert_1) {
            title = "Hey KotlinConf!"
            message = "This codebase is fine. But I think we can do better."
            positive = button("Yeah!")
        }
    }
}

/**
 * A basic plugin, consisting of a unique ID, and various permissions and configuration directives
 */
val samplePlugin2 = plugin("net.sarazan.sample2") {

    /**
     * A plugin usually consists of a card entry,
     * though there can certainly be non-UI plugins (think Services)
     */
    cards = SamplePluginCards2()

    /**
     * Routing logic. Set up absolute URLs or pattern matching,
     * and define a handler action
     */
    handlers {
        handler("foo://ping", pingHandler)
    }

    /**
     * You should not allow plugins to directly call into Android's alert system.
     * Instead you should provide a secure facade that can handle:
     * - Rate limiting
     * - Sequencing
     * - De-duping
     * - content white-listing
     * - security concerns
     */
    alerts {

        /**
         * Toasts are generally innocuous enough that we decided
         * to provide a permissions option to allow unfiltered content
         */
        toasts {
            allowAll()
        }

        /**
         * All alert content must be registered ahead of time in the plugin declaration.
         * It will then be called by ID at run-time.
         */
        alert(R.id.sample_alert_2) {
            title = "Although... it's been a long day."
            message = "We could also just get to the after party a little early?"
            positive = button("Sorry Hadi")
            neutral = button("No Way!", "foo://ping") // Button callbacks are achieved via route handlers.
        }
    }
}