package net.sarazan.host

import android.content.Context
import net.sarazan.plugin.Plugin
import net.sarazan.plugin.host.PluginHostInterface

abstract class HostInterface : PluginHostInterface() {

    lateinit var plugins: List<Plugin>
        private set

    fun initialize(context: Context, plugin: Plugin, plugins: List<Plugin>) {
        this.plugins = plugins
        super.initialize(context, plugin)
    }
}