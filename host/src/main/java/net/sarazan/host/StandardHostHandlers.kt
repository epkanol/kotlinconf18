package net.sarazan.host

import android.content.Context
import android.net.Uri
import net.sarazan.plugin.handlers.HostHandlers
import net.sarazan.plugin.host.PluginHostInterface

class StandardHostHandlers(val c: Context) : HostHandlers {

    override lateinit var host: HostInterface

    override fun initialize(host: PluginHostInterface) {
        this.host = host as HostInterface
    }

    override fun execute(uri: Uri): Boolean {
        val plugin = host.plugin
        val all = host.plugins.asReversed()

        all.forEach {
            val handler = it.handlers?.handlers?.get(uri)
            if (handler?.handle(plugin, uri) == true) return true
        }

        return false
    }
}