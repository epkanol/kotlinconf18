package net.sarazan.plugin.handlers

import android.net.Uri
import net.sarazan.plugin.Plugin

interface PluginUriHandler {
    fun handle(plugin: Plugin, uri: Uri): Boolean
}