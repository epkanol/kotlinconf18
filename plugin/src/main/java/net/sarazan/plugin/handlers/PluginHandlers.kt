package net.sarazan.plugin.handlers

import android.net.Uri
import net.sarazan.plugin.Plugin
import net.sarazan.plugin.internal.PluginComponent
import net.sarazan.plugin.internal.PluginBuilder

/**
 * This is an ultra-simplified version of uri matching for this demo.
 * For now all it can do is a precise match on a full uri.
 *
 * It would not be difficult to upgrade this to match on schemes, hosts, regexes, etc.
 *
 * @see [android.content.UriMatcher]
 */
class PluginHandlers(
        val handlers: Map<Uri, PluginUriHandler>
) : PluginComponent
{
    override lateinit var plugin: Plugin
    override fun initialize(plugin: Plugin) {
        this.plugin = plugin
    }

    @PluginBuilder
    class Builder {

        val handlers = mutableMapOf<Uri, PluginUriHandler>()

        fun handler(uri: String, handler: PluginUriHandler) {
            handler(Uri.parse(uri), handler)
        }

        fun handler(uri: Uri, handler: PluginUriHandler) {
            handlers[uri] = handler
        }

        fun build(): PluginHandlers {
            return PluginHandlers(handlers.toMap())
        }
    }
}