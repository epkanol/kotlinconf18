package net.sarazan.plugin.handlers

import android.net.Uri
import net.sarazan.plugin.internal.HostComponent

interface HostHandlers : HostComponent {
    fun execute(uri: Uri): Boolean
    fun execute(uri: String): Boolean = execute(Uri.parse(uri))
}