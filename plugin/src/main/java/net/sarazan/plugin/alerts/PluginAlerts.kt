package net.sarazan.plugin.alerts

import android.support.annotation.IdRes
import net.sarazan.plugin.Plugin
import net.sarazan.plugin.internal.PluginComponent
import net.sarazan.plugin.internal.PluginBuilder

class PluginAlerts(
        private val alerts: Map<Int, PluginAlert>,
        val toasts: PluginAlertToasts?
) : PluginComponent
{
    override lateinit var plugin: Plugin
    override fun initialize(plugin: Plugin) {
        this.plugin = plugin
    }

    fun findAlert(id: Int) = alerts[id]

    fun show(@IdRes id: Int) {
        val alert = findAlert(id) ?: return
        plugin.host.alerts.show(alert)
    }

    @PluginBuilder
    class Builder {

        private val alerts = mutableListOf<PluginAlert>()

        var toasts: PluginAlertToasts? = null

        fun alert(pluginAlert: PluginAlert) = apply {
            alerts.add(pluginAlert)
        }

        fun build(): PluginAlerts {
            val alertMap = alerts.associateBy { it.id }
            return PluginAlerts(alertMap, toasts)
        }
    }
}