package net.sarazan.plugin.alerts

data class PluginAlertButton(
    val text: String,
    val action: String? = null
)