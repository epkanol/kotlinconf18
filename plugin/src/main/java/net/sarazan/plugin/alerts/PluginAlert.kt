package net.sarazan.plugin.alerts

import android.support.annotation.IdRes
import net.sarazan.plugin.internal.PluginBuilder

data class PluginAlert(
        @IdRes val id: Int,
        var title: String?,
        var message: String?,
        var positive: PluginAlertButton?,
        var neutral: PluginAlertButton?,
        var negative: PluginAlertButton?
) {

    @PluginBuilder
    class Builder(@IdRes val id: Int) {

        var title: String? = null
        fun setTitle(value: String?) = apply { this.title = value }

        var message: String? = null
        fun setMessage(value: String?) = apply { this.message = value }

        var positive: PluginAlertButton? = null
        fun setPositive(value: PluginAlertButton?) = apply { this.positive = value }

        var neutral: PluginAlertButton? = null
        fun setNeutral(value: PluginAlertButton?) = apply { this.neutral = value }

        var negative: PluginAlertButton? = null
        fun setNegative(value: PluginAlertButton?) = apply { this.negative = value }

        fun build(): PluginAlert {
            return PluginAlert(id, title, message, positive, neutral, negative)
        }
    }
}