package net.sarazan.plugin.internal

import net.sarazan.plugin.Plugin

interface PluginComponent {
    val plugin: Plugin
    fun initialize(plugin: Plugin)
}