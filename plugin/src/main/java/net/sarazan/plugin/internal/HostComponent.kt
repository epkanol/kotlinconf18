package net.sarazan.plugin.internal

import net.sarazan.plugin.host.PluginHostInterface

interface HostComponent {
    val host: PluginHostInterface
    fun initialize(host: PluginHostInterface)
}