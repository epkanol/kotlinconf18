package net.sarazan.plugin.cards

import android.support.annotation.LayoutRes
import net.sarazan.plugin.Plugin
import net.sarazan.plugin.internal.PluginComponent

abstract class PluginCards : PluginComponent {

    override lateinit var plugin: Plugin
    override fun initialize(plugin: Plugin) {
        this.plugin = plugin
    }

    @LayoutRes
    abstract fun layout(): Int
    abstract fun onBindViewHolder(p0: PluginCardVH)
}